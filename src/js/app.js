import '../sass/app.sass';
import 'jquery';
import 'bootstrap/dist/js/bootstrap.min';
import 'owl.carousel';
import 'jquery.maskedinput/src/jquery.maskedinput';

const IP = '51.254.85.134';
const PORT = 7776;



const max_index_slides = 4;
const index_slides_interval = 30000;

const goSlider = () => {
    const human = jQuery('.human');
    if (window.index_slider === 0) {
        window.index_slider = 1;
        human.addClass(`human-${window.index_slider}`);
        human.addClass('fade-in');
        setTimeout(() => (goSlider()), index_slides_interval);
    } else {
        human.addClass('fade-out');
        setTimeout(() => {
            human.removeClass(`human-${window.index_slider}`);
            human.removeClass('fade-out');

            window.index_slider++;
            if (window.index_slider >= max_index_slides + 1) window.index_slider = 1;

            human.addClass(`human-${window.index_slider}`);

            setTimeout(() => (goSlider()), index_slides_interval);

        }, 350);
    }
};

window.navbar_active = false;
window.navbar_height = null;

jQuery(document).ready(() => {
    if(window.location.pathname.indexOf('donate.html') !== -1) {
        jQuery('a.navbar-menu__link:contains(Донат)').addClass('navbar-menu__link--active');
    }
    if(window.location.pathname.indexOf('index.html') !== -1 || window.location.pathname === '/') {
        window.index_slider = 0;
        goSlider();
        jQuery('a.navbar-menu__link:contains(Главная)').addClass('navbar-menu__link--active');

        $.ajax({
            type: 'GET',
            url: "https://monitor.sacnr.com/api/?IP=51.254.85.134&Port=7776&Action=info&Format=JSON", // указываем URL
            data: {},
            responseType:'application/json',
            async: false,
            crossDomain: true,
            error: () => {
                jQuery('#players').text(0);
            },
            success: (data) => {
                const response = JSON.parse(data);
                jQuery('#players').text(response.Players);
            }
        });
    }
});


document.getElementsByClassName('navbar-toggler')[0].addEventListener('click', () => {
    const navbar = jQuery('.navbar-menu');
    if (window.navbar_active) {
        document.getElementsByClassName('navbar-menu')[0].classList.remove('navbar-menu--open');
        document.getElementsByClassName('navbar-toggler')[0].classList.remove('navbar-toggler--active');
        document.getElementsByClassName('navbar')[0].classList.remove('navbar--mobile');
        document.body.classList.remove('hidden');
        window.navbar_active = false;
        console.log('window.navbar_height: ', window.navbar_height);
        navbar.animate({ height: window.navbar_height });
        window.navbar_height = null;
    } else {
        if (window.navbar_height == null) {
            window.navbar_height = navbar.height();
        }
        document.getElementsByClassName('navbar-menu')[0].classList.add('navbar-menu--open');
        document.getElementsByClassName('navbar-toggler')[0].classList.add('navbar-toggler--active');
        document.getElementsByClassName('navbar')[0].classList.add('navbar--mobile');
        document.body.classList.add('hidden');
        window.navbar_active = true;
        navbar.animate({ height: window.innerHeight });
    }
});

let Current_X = null;
let Current_Y = null;
const Movement_Power = .4;


$(document).mousemove(function(e) {
    if (!e.pageX || !e.pageY) return 1;
    if(Current_X == null) Current_X = e.pageX;
    if(Current_Y == null) Current_Y = e.pageY;

    const X_Diff = e.pageX - Current_X;
    const Y_Diff = e.pageY - Current_Y;

    Current_X = e.pageX;
    Current_Y = e.pageY;

    const page_overlays = document.querySelectorAll('.page-overlay');

    for (const [page_overlay_idx, page_overlay] of page_overlays.entries()) {
        // if (page_overlay_idx) continue;

        const Movement_Y = (page_overlay_idx + 1) * (Y_Diff * Movement_Power);
        const Movement_X = (page_overlay_idx + 1) * (X_Diff * Movement_Power);

        switch (page_overlay_idx) {
            case 0: {
                page_overlay.style.transform = `translateX(${Movement_X}px) translateY(${Movement_Y}px)`
                break;
            }
            case 1: {
                page_overlay.style.transform = `rotate(116.92deg) translateX(${Movement_X}px) translateY(${Movement_Y}px)`
                break;
            }
            case 2: {
                page_overlay.style.transform = `rotate(-141.26deg) translateX(${Movement_X}px) translateY(${Movement_Y}px)`
                break;
            }
            case 3: {
                page_overlay.style.transform = `rotate(-39.53deg) translateX(${Movement_X}px) translateY(${Movement_Y}px)`
                break;
            }
            case 4: {
                page_overlay.style.transform = `rotate(-122.15deg) translateX(${Movement_X}px) translateY(${Movement_Y}px)`
                break;
            }
        }
    }
});



